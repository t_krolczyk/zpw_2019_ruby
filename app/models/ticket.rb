
class TicketValidator < ActiveModel::Validator
  def validate(record)
    event = Event.find(record.event_id)
    if (event.price_low > record.price) or (event.price_high < record.price)
      record.errors[:base] << "Cena biletu musi mieścić się w przedziale cenowym dla wydarzenia"
    end
  end
end

class Ticket < ApplicationRecord
  belongs_to :event
  belongs_to :user

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :name, :presence => true,
            :length => { :minimum => 6 }
  validates :email, :presence => true,
            format: { with: VALID_EMAIL_REGEX }
  validates :price, :presence => true,
            format: { with: /\A\d+(?:\.\d{2})?\z/ }, numericality: { greater_than: 0, less_than: 1000000 }
  validates :address, :presence => true
  validates :seat_id_seq, :presence => true,
            :length => { :minimum => 2 }
  validates :phone, :presence => true,
            :length => { :minimum => 9 }

  validates_with TicketValidator, fields: [:event_id, :price], :if => :event_id, :presence => true,
                 :if => :price
end
