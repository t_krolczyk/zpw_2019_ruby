require 'date'


class EventValidator < ActiveModel::Validator
  def validate(record)

    if record.event_date < Date.today
      record.errors[:base] << "Data wydarzenia nie może być z przeszłości"
    end

    if record.price_high < record.price_low
      record.errors[:base] << "Dolna granica cenowa nie może być większa od górnej"
    end
  end
end

class Event < ApplicationRecord
  has_many :tickets

  NUM_VALUES = /\A\d+(?:\.\d{2})?\z/

  validates :artist, :presence => true
  validates :price_low, :presence => true,
            format: { with: /\A\d+(?:\.\d{2})?\z/ }, numericality: { greater_than: 0, less_than: 1000000 }
  validates :price_high, :presence => true,
            format: { with: /\A\d+(?:\.\d{2})?\z/ }, numericality: { greater_than: 0, less_than: 1000000 }
  validates :event_date, :presence => true

  validates_with EventValidator, fields: [:event_date, :price_low, :price_high], :if => :event_date, :presence => true,
                 :if => :price_low, :presence => true, :if => :price_high, :presence => true



end

