# Bilecik

**Pierwsze zderzenie z technologiami webowymi - projekt zaliczeniowy z przedmiotu Zaawansowane Technologie Webowe**  
**Technologie:** Ruby on Rails + bootstrap + css3  
**Autentykacja:** 2 rodzaje kont, administratora i użytkownika  

------------------------------------------------------

## Uruchomienie aplikacji w trybie deweloperskim  
1. Aktualizujemy biblioteki zewnętrzne z wykorzystaniem narzędzia bundle:  
	bundle install  
2. Uruchamiamy serwer deweloperski:  
	rails s  
3. Aplikacja została uruchomiona na domyslnym porcie (3000). Adres:  
	http://localhost:3000/  
	
### Przykładowe zrzuty ekranu aplikacji
#### Widok listy wszystkich biletów
![zrzut 1](app/assets/images/ssc/z1.jpg)  
#### Widok strony logowania 
![zrzut 2](app/assets/images/ssc/z2.jpg) 
#### Widok strony rejestracji
![zrzut 3](app/assets/images/ssc/z5.jpg)  
#### Widok menu edycji biletu
![zrzut 4](app/assets/images/ssc/z4.jpg)  
#### Widok szczegółowy wydarzenia 
![zrzut 5](app/assets/images/ssc/z3.jpg) 
