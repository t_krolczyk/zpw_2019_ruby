Rails.application.routes.draw do
  devise_for :users
  resources :events, :only => [:index, :new, :create, :show]
  post '/events'    => 'events#create'
  resources :tickets
  get 'home/hello'
  root :to => 'events#index'
end
